<?php

namespace App\Repositories;

use App\Model\ProjectMilestone;
use DB;


class LaporanRepository
{
    public function getProject($q)
    {
        $model = DB::table('projects');

        if (!empty($q)) {
            $model->where('name', 'LIKE', '%'.$q.'%');
        }

        return $model;
    }

    public function getJsonLaporanProject($project_id)
    {
        $milestone = ProjectMilestone::where('project_id',$project_id)->get();

        $data = [];
        foreach ($milestone as $value) {

            $data[] = [
                'name' => $value->name,
                'y' => $value->getPercentage($value->id)
            ];
        }

        return json_encode($data);
    }

    public function getJsonLaporanPegawai($id)
    {
        $employee = $this->getLaporanEmployee($id)->get();

        $data = [];
        foreach ($employee as $value) {
            $data[] = [
                'name' => $value->name,
                'y' => (int)$this->getLoad($value)
            ];
        }

        return json_encode($data);
    }

    public function getEmployee($q)
    {
        $model = DB::table('employees');

        if (!empty($q)) {
            $model->where('name', 'LIKE', '%'.$q.'%');
        }

        return $model;
    }

    public function getLaporanEmployee($id)
    {
        $query = DB::table('project_to_users AS ptu')
                    ->select(
                        'ptu.project_id', 'ptu.employee_id', 'p.name',
                        DB::raw('COUNT(1) as jumlah_task'),
                        DB::raw(
                            '(
                                SELECT COUNT(1) FROM project_milestones pmi
                                JOIN features fa ON fa.milestone_id = pmi.id
                                JOIN tasks ta ON ta.feature_id = fa.id
                                WHERE pmi.project_id = p.id and ta.employee_id = '.$id.'
                            ) AS jumlah_task_assign'
                        ),
                        DB::raw(
                            '(
                                select count(1) from project_milestones pmi
                                JOIN features fa ON fa.milestone_id = pmi.id
                                JOIN tasks ta ON ta.feature_id = fa.id
                                WHERE pmi.project_id = p.id and ta.`status` = \'done\'
                            ) AS jumlah_task_done'
                        )
                    )
                    ->join('projects AS p',  'p.id', '=', 'ptu.project_id')
                    ->join('project_milestones AS pm',  'pm.project_id', '=', 'p.id')
                    ->join('features AS f',  'f.milestone_id', '=', 'pm.id')
                    ->join('tasks AS t',  't.feature_id', '=', 'f.id')
                    ->where('ptu.employee_id', $id)
                    ->groupBy('ptu.project_id');

        return $query;
    }

    public static function getProgress($value)
    {
        $total = ($value->jumlah_task_done / $value->jumlah_task) * 100;

        return number_format($total);
    }

    public static function getLoad($value)
    {
        $total = ($value->jumlah_task_assign / $value->jumlah_task) * 100;

        return number_format($total);
    }
}
