<?php

namespace App\Repositories;

use App\Enum\TaskEnum;
use App\Http\Requests\MilestoneStore;
use App\Model\Employee;
use App\Model\Feature;
use App\Model\ProjectMilestone;
use App\Model\ProjectToUser;
use App\Model\Task;
use Carbon\Carbon;
use DB;


class FeatureRepository
{
    public function __construct(Feature $model)
    {
        $this->model = $model;
    }

    public function getEmployee($milestone_id)
    {
        $milestone = ProjectMilestone::find($milestone_id);

        $user_ids = ProjectToUser::where('project_id', $milestone->project_id)->get()->pluck('employee_id')->toArray();

        $employee = Employee::findMany($user_ids)->pluck('name', 'id')->toArray();

        return $employee;
    }

    public function createNew(array $attributes, $milestone_id)
    {
        $model                  = new Feature();
        $model->milestone_id    = $milestone_id;
        $model->name            = $attributes['name'];
        $model->due_date        = Carbon::parse($attributes['due_date'])->format('Y-m-d');

        $model->save();

        foreach ($attributes['task_name'] as $key => $value) {
            $task = new Task([
                'name'        => $value,
                'status'      => TaskEnum::NOT_DONE,
                'employee_id' => $attributes['employee_id'][$key]
            ]);

            $model->tasks()->save($task);
        }

        return $model;
    }

    public function updateOld(Feature $model, array $attributes)
    {

        $model->name        = $attributes['name'];
        $model->due_date    = Carbon::parse($attributes['due_date'])->format('Y-m-d');
//        $model->status      = $attributes['status'];
        $model->save();

        Task::where('feature_id', $model->id)->delete();

        foreach ($attributes['task_name'] as $key => $value) {
            $task = new Task(['name' => $value]);

            $model->tasks()->save($task);
        }

        return $model;
    }
}
