<?php

namespace App\Repositories;

use App\Enum\TaskEnum;
use App\Model\Feature;
use App\Model\Task;
use DB;


class TaskRepository
{
    public function __construct(Task $model)
    {
        $this->model = $model;
    }

    public function createNew(array $attributes, $feature_id)
    {

        $model                  = new Task();
        $model->feature_id      = $feature_id;
        $model->name            = $attributes['name'];
        $model->save();

        return $model;
    }

    public function updateOld(Task $model, array $attributes)
    {
        $model->name            = $attributes['name'];
        $model->status          = $attributes['status'];
        $model->save();


        $count_task_done = Task::where('feature_id', $model->feature_id)->where('status', TaskEnum::DONE)->count();

        $count_task_all  = Task::where('feature_id', $model->feature_id)->count();

        $feature = Feature::find($model->feature_id);
        $feature->percentage = $count_task_done / $count_task_all * 100;
        $feature->save();


        return $model;
    }
}
