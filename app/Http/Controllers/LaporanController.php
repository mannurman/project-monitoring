<?php

namespace App\Http\Controllers;

use App\Model\Employee;
use App\Model\Project;
use App\Model\ProjectMilestone;
use Illuminate\Http\Request;
use App\Repositories\LaporanRepository;

class LaporanController extends Controller
{
    /**
     * @var Project
     */
    protected $repository;

    public function __construct(LaporanRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getProject(Request $request)
    {
        $q = $request->get('q');

        $items = $this->repository->getProject($q)->paginate();

        return view('laporan.project.index', compact('items', 'q'));
    }

    public function getShowProject($slug)
    {
        $item = Project::findBySlug($slug);

        $milestones = ProjectMilestone::where('project_id', $item->id)->paginate();

        $json_data = $this->repository->getJsonLaporanProject($item->id);

        return view('laporan.project.show', compact('item', 'milestones', 'json_data'));
    }

    public function getEmployee(Request $request)
    {
        $q = $request->get('q');

        $items = $this->repository->getEmployee($q)->paginate();

        return view('laporan.pegawai.index', compact('items', 'q'));
    }

    public function getShowEmployee($id)
    {
        $item = Employee::find($id);

        $project = $this->repository->getLaporanEmployee($id)->paginate();

        $json_data = $this->repository->getJsonLaporanPegawai($id);

        return view('laporan.pegawai.show', compact('item','json_data', 'project'));
    }
}
