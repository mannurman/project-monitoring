<?php

namespace App\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'client_name'
            ]
        ];
    }

    protected $fillable = ['client_name'];

    public function projects()
    {
        return $this->hasMany('App\Model\Project', 'client_id');
    }
}
