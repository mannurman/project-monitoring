<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $fillable = ['name', 'status'];

    public function tasks()
    {
        return $this->hasMany('App\Model\Task', 'feature_id', 'id');
    }
}
