<?php

namespace App\Model;

use App\Enum\TaskEnum;
use Illuminate\Database\Eloquent\Model;

class ProjectMilestone extends Model
{
    protected $fillable = ['name', 'start_date', 'end_date'];

    public function project()
    {
        return $this->belongsTo('App\Model\Project', 'project_id');
    }

    public function feature()
    {
        return $this->hasMany('App\Model\Feature', 'milestone_id', 'id');
    }

    public function getPercentage($milestone_id)
    {
        $feature_ids = Feature::where('milestone_id', $milestone_id)->get()->pluck('id')->toArray();

        $all_task  = Task::whereIn('feature_id', $feature_ids)->count();
        $done_task = Task::whereIn('feature_id', $feature_ids)->where('status', TaskEnum::DONE)->count();

        if($done_task > 0) {
            $total = ($done_task / $all_task) * 100;

            return (int)$total;
        }

        return 0;
    }
}
