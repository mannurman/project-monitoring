<?php

namespace App\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    protected $fillable = ['name', 'scope', 'bahasa_prog', 'database', 'server', 'other', 'description'];

    public function client()
    {
    	return $this->belongsTo('App\Model\Client', 'client_id');
    }

    public function milestones()
    {
        return $this->hasMany('App\Model\ProjectMilestone', 'project_id');
    }
}
