<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['name', 'employee_id', 'status'];

    public function feature()
    {
        return $this->belongsTo('App\Model\Feature', 'feature_id');
    }

    public function employee()
    {
        return $this->belongsTo('App\Model\Employee', 'employee_id');
    }
}
