<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Login</title>


    <link href="{!! asset('css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('font-awesome/css/font-awesome.css') !!}" rel="stylesheet">

    <link href="{!! asset('css/style.min.css') !!}" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="loginColumns animated fadeInDown">
        <div class="row">
            <div class="col-md-12" style="padding-bottom: 50px;">
                <h1 class="font-bold text-center">Project Monitoring</h1>
            </div>

            <div class="col-md-6">
                <img src="{!! asset('img/logo-javan2.png') !!}" >

            </div>
            <div class="col-md-6">
                <div class="ibox-content">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="m-t" role="form" method="post" action="/auth/login">

                        {!! csrf_field() !!}

                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="Email" required="" value="{{ old('email') }}">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password" required="">
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                    </form>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Javan Labs
            </div>
            <div class="col-md-6 text-right">
               <small>&copy; 2015</small>
            </div>
        </div>
    </div>

</body>

</html>
