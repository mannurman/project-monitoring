@extends('master.default')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Feature List</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{!! route('dashboard.index') !!}">Home</a>
                </li>
                <li class="active">
                    <strong>Feature List</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeIn">
        <div class="ibox">
            <div class="ibox-title">
                <form>
                <div class="input-group">
                    {!! Form::text('q', $q, ['class' => 'form-control', 'placeholder' => 'Cari Project']) !!}

                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">Cari</button>
                    </span>
                </div>
                </form>
            </div>

            <div class="ibox-content">
                <div class="project-list">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Client</th>
                                <th width="220"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $row)
                                <tr>
                                    <td>{{ $row->name }}</td>
                                    <td>{{ $row->client->client_name ?? null }}</td>
                                    <td class="project-actions">
                                        <a href="{!! route('laporan.project.show', ['id' => $row->slug]) !!}" class="btn btn-white btn-sm">Laporan</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! $items->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
