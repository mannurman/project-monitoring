@extends('master.default')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Pegawai List</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{!! route('dashboard.index') !!}">Home</a>
                </li>
                <li class="active">
                    <strong>Pegawai List</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeIn">
        <div class="ibox">
            <div class="ibox-title">
                <form>
                    <div class="input-group">
                        {!! Form::text('q', $q, ['class' => 'form-control', 'placeholder' => 'Cari Pegawai']) !!}

                        <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">Cari</button>
                    </span>
                    </div>
                </form>
            </div>

            <div class="ibox-content">
                <div class="project-list">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>NIP</th>
                                <th>Name</th>
                                <th width="220"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $row)
                                <tr>
                                    <td>{{ $row->nip }}</td>
                                    <td>{{ $row->name }}</td>
                                    <td class="project-actions">
                                        <a href="{!! route('laporan.employee.show', ['id' => $row->id]) !!}" class="btn btn-white btn-sm">Laporan</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! $items->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
