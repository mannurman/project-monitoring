@extends('master.default')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Laporan  Pegawai</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="#">Home</a>
                </li>
                <li class="active">
                    <strong>Laporan Pegawai</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <div class="wrapper wrapper-content">
                <div class="ibox">
                    <div class="ibox-content">

                        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

                        <div class="row m-t-sm">
                            <div class="col-lg-12">
                                <div class="panel blank-panel">

                                    <hr>

                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Project</th>
                                                    <th>Progress</th>
                                                    <th>Load</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($project as $row)
                                                    <tr>
                                                        <td>{{ $row->name  }}</td>
                                                        <td>{{ \App\Repositories\LaporanRepository::getProgress($row)}} %</td>
                                                        <td>{{ \App\Repositories\LaporanRepository::getLoad($row) }} %</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('front.javascript')
<script type="text/javascript">
    $(function () {
        // Create the chart
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Laporan Persentase Beban Kerja {{ $item->name }} Per-Projek'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total percent Beban Pekerjaan Per-projek'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: {!! $json_data !!}
            }]
        });
    });
</script>
<script type="text/javascript">



    $(document).ready(function(){
        $("#start-date").datepicker({
            format: 'dd-mm-yyyy',
            startDate: '1d'
        }).on('show', function(e) {
            $('#end-date').datepicker("setStartDate", e.date);
        });

        $("#end-date").datepicker({
            format: 'dd-mm-yyyy',
            startDate: '1d'
        }).on('show', function(e) {
            $('#start-date').datepicker("setEndDate", e.date);
        });
    });
</script>

@endpush

