<div class="form-group">
    {!! Form::label('name', 'Name', array('class' => 'col-sm-2 control-label')) !!}

    <div class="col-sm-10">
        {!! Form::text('name', $item->name ?? null, array('class' => 'form-control')) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('due_date', 'Due Date', array('class' => 'col-sm-2 control-label')) !!}

    <div class="col-sm-10">
        {!! Form::text('due_date', $item->due_date ?? null, array('class' => 'form-control datepicker')) !!}
    </div>
</div>

{{--<div class="form-group">--}}
    {{--{!! Form::label('status', 'Status', array('class' => 'col-sm-2 control-label')) !!}--}}

    {{--<div class="col-sm-10">--}}
        {{--{!! Form::select('status', ['not active', 'active'], $item->status ?? null, array('class' => 'form-control')) !!}--}}
    {{--</div>--}}
{{--</div>--}}
<div id="form-task">
    @if(!empty($item->tasks))

        @foreach($item->tasks as $key => $task)

            @if($key == 0)

                <div class="form-group">
                    {!! Form::label('due_date', 'Tasks', array('class' => 'col-sm-2 control-label')) !!}

                    <div class="col-sm-4">
                        <input type="text" name="task_name[]" value="{{ $task->name }}" class="form-control input-xs" placeholder="Task Name">
                    </div>

                    <div class="col-sm-4">
                        {!! Form::select('employee_id', $employee, $task->employee_id ?? null, array('class' => 'form-control')) !!}
                    </div>

                    <div class="col-sm-1">
                        <button type="button" id="btn-task" class="btn btn-sm btn-primary" style="text-align: center"><span class="glyphicon glyphicon-plus"></span></button>
                    </div>
                </div>
            @else
                <div class="form-group">
                    <div class="col-sm-2"></div>

                    <div class="col-sm-4">
                        <input type="text" name="task_name[]" value="{{ $task->name }}" class="form-control input-xs" placeholder="Task Name">
                    </div>

                    <div class="col-sm-4">
                        {!! Form::select('employee_id', $employee, $task->employee_id ?? null, array('class' => 'form-control')) !!}
                    </div>

                    <div class="col-sm-1">
                        <button type="button" class="btn btn-sm btn-danger btn-task-delete" style="text-align: center"><span class="glyphicon glyphicon-minus"></span></button>
                    </div>
                </div>
            @endif
        @endforeach
    @else
        <div class="form-group">
            {!! Form::label('due_date', 'Tasks', array('class' => 'col-sm-2 control-label')) !!}
            <div class="col-sm-4">
                <input type="text" name="task_name[]" class="form-control input-xs" placeholder="Task Name">
            </div>
            <div class="col-sm-4">
                {!! Form::select('employee_id[]', $employee, null, array('class' => 'form-control')) !!}
            </div>
            <div class="col-sm-1">
                <button type="button" id="btn-task" class="btn btn-sm btn-primary" style="text-align: center"><span class="glyphicon glyphicon-plus"></span></button>
            </div>
        </div>

    @endif
</div>

<div class="hr-line-dashed"></div>

<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
        {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
        <button class="btn btn-white" type="button">Cancel</button>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#btn-task").on('click', function () {
            var html =  '<div class="form-group field-task">'+
                            '<div class="col-sm-2"></div>'+
                            '<div class="col-sm-4">'+
                                '<input type="text" name="task_name[]" class="form-control input-xs" placeholder="Task Name">'+
                            '</div>'+
                            '<div class="col-sm-4">'+
                                '{!! Form::select('employee_id[]', $employee, null, array('class' => 'form-control')) !!}'+
                            '</div>'+
                            '<div class="col-sm-1">'+
                                '<button type="button" class="btn btn-sm btn-danger btn-task-delete" style="text-align: center"><span class="glyphicon glyphicon-minus"></span></button>'+
                            '</div>'+
                        '</div>';

            $("#form-task").append(html);
        });

        $("#form-task").on("click", '.btn-task-delete', function(e){ //user click on remove text
            $(this).parent().parent().remove();
        });
    });
</script>